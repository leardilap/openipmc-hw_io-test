/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#ifndef HSEM_ID_0
#define HSEM_ID_0 (0U) /* HW semaphore 0*/
#endif
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
void StartDefaultTask(void *argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
/* USER CODE BEGIN Boot_Mode_Sequence_0 */
  int32_t timeout;
/* USER CODE END Boot_Mode_Sequence_0 */

/* USER CODE BEGIN Boot_Mode_Sequence_1 */
  /* Wait until CPU2 boots and enters in stop mode or timeout*/
//  timeout = 0xFFFF;
//  while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) != RESET) && (timeout-- > 0));
//  if ( timeout < 0 )
//  {
//  Error_Handler();
//  }
/* USER CODE END Boot_Mode_Sequence_1 */
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();
/* USER CODE BEGIN Boot_Mode_Sequence_2 */
/* When system initialization is finished, Cortex-M7 will release Cortex-M4 by means of
HSEM notification */
/*HW semaphore Clock enable*/
__HAL_RCC_HSEM_CLK_ENABLE();
/*Take HSEM */
HAL_HSEM_FastTake(HSEM_ID_0);
/*Release HSEM in order to notify the CPU2(CM4)*/
HAL_HSEM_Release(HSEM_ID_0,0);
/* wait until CPU2 wakes up from stop mode */
timeout = 0xFFFF;
while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) == RESET) && (timeout-- > 0));
if ( timeout < 0 )
{
Error_Handler();
}
/* USER CODE END Boot_Mode_Sequence_2 */

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_SMPS_1V8_SUPPLIES_LDO);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV1;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOI_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOK_CLK_ENABLE();
  __HAL_RCC_GPIOJ_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOI, I2C_BB_SCL_Pin|I2C_BB_SDA_Pin|IPM_0_Pin|IPM_1_Pin
                          |ETH_RST_N_Pin|IPM_3_Pin|IPM_4_Pin|IPM_5_Pin
                          |USR_IO_10_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, USR_IO_6_Pin|IPMB_A_SCL_Pin|IPMB_A_SDA_Pin|DIMM_UART0_TX_Pin
                          |USR_IO_27_Pin|USR_IO_14_Pin|PYLD_RESET_Pin|USR_IO_12_Pin
                          |PWR_GOOD_B_Pin|USR_IO_33_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, USR_IO_0_Pin|USR_IO_1_Pin|IPM_11_Pin|USR_IO_23_Pin
                          |USR_IO_25_Pin|USR_IO_28_Pin|USR_IO_29_Pin|USR_IO_20_Pin
                          |USR_IO_24_Pin|USR_IO_22_Pin|USR_IO_26_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOH, DIMM_UART0_RX_Pin|USR_IO_3_Pin|ETH_TX_ER_Pin|HW_2_Pin
                          |MASTER_TCK_Pin|HW_0_Pin|PWR_GOOD_A_Pin|USR_IO_32_Pin
                          |USR_IO_30_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, DIMM_UART1_TX_Pin|ETH_INT_N_Pin|I2C3_SDA_Pin|IPM_13_Pin
                          |IPM_8_Pin|USR_IO_2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, IPM_14_Pin|USR_IO_7_Pin|IPM_7_Pin|IPMB_B_EN_Pin
                          |IPMB_B_RDY_Pin|IPMB_A_EN_Pin|USR_IO_21_Pin|USR_IO_17_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, DIMM_UART1_RX_Pin|ALARM_B_Pin|MASTER_TDO_Pin|EN_12V_Pin
                          |FP_LED_2_Pin|MASTER_TRST_Pin|FP_LED_0_Pin|FP_LED_1_Pin
                          |FP_LED_BLUE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, USR_IO_34_Pin|I2C3_SCL_Pin|IPM_10_Pin|USR_IO_4_Pin
                          |USR_IO_5_Pin|USR_IO_8_Pin|USR_IO_9_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOF, IPM_2_Pin|IPMB_B_SCL_Pin|IPMB_B_SDA_Pin|IPM_6_Pin
                          |IPM_12_Pin|IPM_9_Pin|ALARM_A_Pin|MGM_SCL_Pin
                          |USR_IO_18_Pin|MGM_SDA_Pin|USR_IO_19_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOK, IPMB_A_RDY_Pin|HW_5_Pin|HW_7_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOJ, HW_3_Pin|HW_1_Pin|HW_6_Pin|IPM_15_Pin
                          |HW_4_Pin|MASTER_TDI_Pin|MASTER_TMS_Pin|USR_IO_11_Pin
                          |USR_IO_31_Pin|USR_IO_15_Pin|USR_IO_13_Pin|USR_IO_16_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : I2C_BB_SCL_Pin I2C_BB_SDA_Pin IPM_0_Pin IPM_1_Pin
                           ETH_RST_N_Pin IPM_3_Pin IPM_4_Pin IPM_5_Pin
                           USR_IO_10_Pin */
  GPIO_InitStruct.Pin = I2C_BB_SCL_Pin|I2C_BB_SDA_Pin|IPM_0_Pin|IPM_1_Pin
                          |ETH_RST_N_Pin|IPM_3_Pin|IPM_4_Pin|IPM_5_Pin
                          |USR_IO_10_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pins : USR_IO_6_Pin IPMB_A_SCL_Pin IPMB_A_SDA_Pin DIMM_UART0_TX_Pin
                           USR_IO_27_Pin USR_IO_14_Pin PYLD_RESET_Pin USR_IO_12_Pin
                           PWR_GOOD_B_Pin USR_IO_33_Pin */
  GPIO_InitStruct.Pin = USR_IO_6_Pin|IPMB_A_SCL_Pin|IPMB_A_SDA_Pin|DIMM_UART0_TX_Pin
                          |USR_IO_27_Pin|USR_IO_14_Pin|PYLD_RESET_Pin|USR_IO_12_Pin
                          |PWR_GOOD_B_Pin|USR_IO_33_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : USR_IO_0_Pin USR_IO_1_Pin IPM_11_Pin USR_IO_23_Pin
                           USR_IO_25_Pin USR_IO_28_Pin USR_IO_29_Pin USR_IO_20_Pin
                           USR_IO_24_Pin USR_IO_22_Pin USR_IO_26_Pin */
  GPIO_InitStruct.Pin = USR_IO_0_Pin|USR_IO_1_Pin|IPM_11_Pin|USR_IO_23_Pin
                          |USR_IO_25_Pin|USR_IO_28_Pin|USR_IO_29_Pin|USR_IO_20_Pin
                          |USR_IO_24_Pin|USR_IO_22_Pin|USR_IO_26_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : DIMM_UART0_RX_Pin USR_IO_3_Pin ETH_TX_ER_Pin HW_2_Pin
                           MASTER_TCK_Pin HW_0_Pin PWR_GOOD_A_Pin USR_IO_32_Pin
                           USR_IO_30_Pin */
  GPIO_InitStruct.Pin = DIMM_UART0_RX_Pin|USR_IO_3_Pin|ETH_TX_ER_Pin|HW_2_Pin
                          |MASTER_TCK_Pin|HW_0_Pin|PWR_GOOD_A_Pin|USR_IO_32_Pin
                          |USR_IO_30_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  /*Configure GPIO pins : DIMM_UART1_TX_Pin ETH_INT_N_Pin I2C3_SDA_Pin IPM_13_Pin
                           IPM_8_Pin USR_IO_2_Pin */
  GPIO_InitStruct.Pin = DIMM_UART1_TX_Pin|ETH_INT_N_Pin|I2C3_SDA_Pin|IPM_13_Pin
                          |IPM_8_Pin|USR_IO_2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : IPM_14_Pin USR_IO_7_Pin IPM_7_Pin IPMB_B_EN_Pin
                           IPMB_B_RDY_Pin IPMB_A_EN_Pin USR_IO_21_Pin USR_IO_17_Pin */
  GPIO_InitStruct.Pin = IPM_14_Pin|USR_IO_7_Pin|IPM_7_Pin|IPMB_B_EN_Pin
                          |IPMB_B_RDY_Pin|IPMB_A_EN_Pin|USR_IO_21_Pin|USR_IO_17_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pins : DIMM_UART1_RX_Pin ALARM_B_Pin MASTER_TDO_Pin EN_12V_Pin
                           FP_LED_2_Pin MASTER_TRST_Pin FP_LED_0_Pin FP_LED_1_Pin
                           FP_LED_BLUE_Pin */
  GPIO_InitStruct.Pin = DIMM_UART1_RX_Pin|ALARM_B_Pin|MASTER_TDO_Pin|EN_12V_Pin
                          |FP_LED_2_Pin|MASTER_TRST_Pin|FP_LED_0_Pin|FP_LED_1_Pin
                          |FP_LED_BLUE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : USR_IO_34_Pin I2C3_SCL_Pin IPM_10_Pin USR_IO_4_Pin
                           USR_IO_5_Pin USR_IO_8_Pin USR_IO_9_Pin */
  GPIO_InitStruct.Pin = USR_IO_34_Pin|I2C3_SCL_Pin|IPM_10_Pin|USR_IO_4_Pin
                          |USR_IO_5_Pin|USR_IO_8_Pin|USR_IO_9_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : IPM_2_Pin IPMB_B_SCL_Pin IPMB_B_SDA_Pin IPM_6_Pin
                           IPM_12_Pin IPM_9_Pin ALARM_A_Pin MGM_SCL_Pin
                           USR_IO_18_Pin MGM_SDA_Pin USR_IO_19_Pin */
  GPIO_InitStruct.Pin = IPM_2_Pin|IPMB_B_SCL_Pin|IPMB_B_SDA_Pin|IPM_6_Pin
                          |IPM_12_Pin|IPM_9_Pin|ALARM_A_Pin|MGM_SCL_Pin
                          |USR_IO_18_Pin|MGM_SDA_Pin|USR_IO_19_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : IPMB_A_RDY_Pin HW_5_Pin HW_7_Pin */
  GPIO_InitStruct.Pin = IPMB_A_RDY_Pin|HW_5_Pin|HW_7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOK, &GPIO_InitStruct);

  /*Configure GPIO pins : HW_3_Pin HW_1_Pin HW_6_Pin IPM_15_Pin
                           HW_4_Pin MASTER_TDI_Pin MASTER_TMS_Pin USR_IO_11_Pin
                           USR_IO_31_Pin USR_IO_15_Pin USR_IO_13_Pin USR_IO_16_Pin */
  GPIO_InitStruct.Pin = HW_3_Pin|HW_1_Pin|HW_6_Pin|IPM_15_Pin
                          |HW_4_Pin|MASTER_TDI_Pin|MASTER_TMS_Pin|USR_IO_11_Pin
                          |USR_IO_31_Pin|USR_IO_15_Pin|USR_IO_13_Pin|USR_IO_16_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOJ, &GPIO_InitStruct);

  /*Configure GPIO pin : HANDLE_Pin */
  GPIO_InitStruct.Pin = HANDLE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(HANDLE_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for(;;)
  {
	  if(HAL_GPIO_ReadPin(HANDLE_GPIO_Port, HANDLE_Pin)) // PB12
	  {
		  HAL_GPIO_WritePin(EN_12V_GPIO_Port, EN_12V_Pin, GPIO_PIN_SET); // PD11
	  }
	  else
	  {
		  //HAL_GPIO_WritePin(EN_12V_GPIO_Port, EN_12V_Pin, GPIO_PIN_RESET); // PD11
		  HAL_GPIO_TogglePin(EN_12V_GPIO_Port, EN_12V_Pin); // PD11
	  }
	  osDelay(100);
	  HAL_GPIO_TogglePin(ALARM_A_GPIO_Port, ALARM_A_Pin); // PF13
	  osDelay(10);
	  HAL_GPIO_TogglePin(ALARM_B_GPIO_Port, ALARM_B_Pin); // PD15
	  osDelay(10);
	  HAL_GPIO_TogglePin(DIMM_UART0_RX_GPIO_Port, DIMM_UART0_RX_Pin); // PH14
	  osDelay(10);
	  HAL_GPIO_TogglePin(DIMM_UART0_TX_GPIO_Port, DIMM_UART0_TX_Pin); // PB9
	  osDelay(10);
	  HAL_GPIO_TogglePin(DIMM_UART1_RX_GPIO_Port, DIMM_UART1_RX_Pin); // PD2
	  osDelay(10);
	  HAL_GPIO_TogglePin(DIMM_UART1_TX_GPIO_Port, DIMM_UART1_TX_Pin); // PC12
	  osDelay(10);
	  HAL_GPIO_TogglePin(ETH_INT_N_GPIO_Port, ETH_INT_N_Pin); // PC13
	  osDelay(10);
	  HAL_GPIO_TogglePin(ETH_RST_N_GPIO_Port, ETH_RST_N_Pin); // PI8
	  osDelay(10);
	  HAL_GPIO_TogglePin(ETH_TX_ER_GPIO_Port, ETH_TX_ER_Pin); // PH5
	  osDelay(10);
	  HAL_GPIO_TogglePin(FP_LED_0_GPIO_Port, FP_LED_0_Pin); // PD10
	  osDelay(10);
	  HAL_GPIO_TogglePin(FP_LED_1_GPIO_Port, FP_LED_1_Pin); // PD9
	  osDelay(10);
	  HAL_GPIO_TogglePin(FP_LED_2_GPIO_Port, FP_LED_2_Pin); // PD12
	  osDelay(10);
	  HAL_GPIO_TogglePin(FP_LED_BLUE_GPIO_Port, FP_LED_BLUE_Pin); // PD8
	  osDelay(10);
	  HAL_GPIO_TogglePin(HW_0_GPIO_Port, HW_0_Pin); // PH9
	  osDelay(10);
	  HAL_GPIO_TogglePin(HW_1_GPIO_Port, HW_1_Pin); // PJ10
	  osDelay(10);
	  HAL_GPIO_TogglePin(HW_2_GPIO_Port, HW_2_Pin); // PH10
	  osDelay(10);
	  HAL_GPIO_TogglePin(HW_3_GPIO_Port, HW_3_Pin); // PJ11
	  osDelay(10);
	  HAL_GPIO_TogglePin(HW_4_GPIO_Port, HW_4_Pin); // PJ8
	  osDelay(10);
	  HAL_GPIO_TogglePin(HW_5_GPIO_Port, HW_5_Pin); // PK0
	  osDelay(10);
	  HAL_GPIO_TogglePin(HW_6_GPIO_Port, HW_6_Pin); // PJ9
	  osDelay(10);
	  HAL_GPIO_TogglePin(HW_7_GPIO_Port, HW_7_Pin); // PK1
	  osDelay(10);
	  HAL_GPIO_TogglePin(I2C_BB_SCL_GPIO_Port, I2C_BB_SCL_Pin); // PI6
	  osDelay(10);
	  HAL_GPIO_TogglePin(I2C_BB_SDA_GPIO_Port, I2C_BB_SDA_Pin); // PI5
	  osDelay(10);
	  HAL_GPIO_TogglePin(I2C3_SCL_GPIO_Port, I2C3_SCL_Pin); // PA8
	  osDelay(10);
	  HAL_GPIO_TogglePin(I2C3_SDA_GPIO_Port, I2C3_SDA_Pin); // PC9
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_0_GPIO_Port, IPM_0_Pin); // PI4
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_1_GPIO_Port, IPM_1_Pin); // PI7
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_2_GPIO_Port, IPM_2_Pin); // PF2
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_3_GPIO_Port, IPM_3_Pin); // PI12
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_4_GPIO_Port, IPM_4_Pin); // PI13
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_5_GPIO_Port, IPM_5_Pin); // PI14
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_6_GPIO_Port, IPM_6_Pin); // PF3
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_7_GPIO_Port, IPM_7_Pin); // PG5
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_8_GPIO_Port, IPM_8_Pin); // PC6
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_9_GPIO_Port, IPM_9_Pin); // PF4
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_10_GPIO_Port, IPM_10_Pin); // PA0
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_11_GPIO_Port, IPM_11_Pin); // PE10
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_12_GPIO_Port, IPM_12_Pin); // PF5
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_13_GPIO_Port, IPM_13_Pin); // PC7
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_14_GPIO_Port, IPM_14_Pin); // PG14
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPM_15_GPIO_Port, IPM_15_Pin); // PJ0
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPMB_A_EN_GPIO_Port, IPMB_A_EN_Pin); // PG2
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPMB_A_RDY_GPIO_Port, IPMB_A_RDY_Pin); // PK2
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPMB_A_SCL_GPIO_Port, IPMB_A_SCL_Pin); // PB6
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPMB_A_SDA_GPIO_Port, IPMB_A_SDA_Pin); // PB7
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPMB_B_EN_GPIO_Port, IPMB_B_EN_Pin); // PG4
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPMB_B_RDY_GPIO_Port, IPMB_B_RDY_Pin); // PG3
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPMB_B_SCL_GPIO_Port, IPMB_B_SCL_Pin); // PF1
	  osDelay(10);
	  HAL_GPIO_TogglePin(IPMB_B_SDA_GPIO_Port, IPMB_B_SDA_Pin); // PF0
	  osDelay(10);
	  HAL_GPIO_TogglePin(MASTER_TCK_GPIO_Port, MASTER_TCK_Pin); // PH11
	  osDelay(10);
	  HAL_GPIO_TogglePin(MASTER_TDI_GPIO_Port, MASTER_TDI_Pin); // PJ7
	  osDelay(10);
	  HAL_GPIO_TogglePin(MASTER_TDO_GPIO_Port, MASTER_TDO_Pin); // PD14
	  osDelay(10);
	  HAL_GPIO_TogglePin(MASTER_TMS_GPIO_Port, MASTER_TMS_Pin); // PJ6
	  osDelay(10);
	  HAL_GPIO_TogglePin(MASTER_TRST_GPIO_Port, MASTER_TRST_Pin); // PD13
	  osDelay(10);
	  HAL_GPIO_TogglePin(MGM_SCL_GPIO_Port, MGM_SCL_Pin); // PF14
	  osDelay(10);
	  HAL_GPIO_TogglePin(MGM_SDA_GPIO_Port, MGM_SDA_Pin); // PF15
	  osDelay(10);
	  HAL_GPIO_TogglePin(PWR_GOOD_A_GPIO_Port, PWR_GOOD_A_Pin); // PH12
	  osDelay(10);
	  HAL_GPIO_TogglePin(PWR_GOOD_B_GPIO_Port, PWR_GOOD_B_Pin); // PB13
	  osDelay(10);
	  HAL_GPIO_TogglePin(PYLD_RESET_GPIO_Port, PYLD_RESET_Pin); // PB15
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_0_GPIO_Port, USR_IO_0_Pin); // PE1
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_1_GPIO_Port, USR_IO_1_Pin); // PE0
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_2_GPIO_Port, USR_IO_2_Pin); // PC0
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_3_GPIO_Port, USR_IO_3_Pin); // PH4
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_4_GPIO_Port, USR_IO_4_Pin); // PA6
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_5_GPIO_Port, USR_IO_5_Pin); // PA5
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_6_GPIO_Port, USR_IO_6_Pin); // PB5
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_7_GPIO_Port, USR_IO_7_Pin); // PG8
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_8_GPIO_Port, USR_IO_8_Pin); // PA3
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_9_GPIO_Port, USR_IO_9_Pin); // PA4
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_10_GPIO_Port, USR_IO_10_Pin); // PI15
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_11_GPIO_Port, USR_IO_11_Pin); // PJ1
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_12_GPIO_Port, USR_IO_12_Pin); // PB0
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_13_GPIO_Port, USR_IO_13_Pin); // PJ3
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_14_GPIO_Port, USR_IO_14_Pin); // PB2
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_15_GPIO_Port, USR_IO_15_Pin); // PJ2
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_16_GPIO_Port, USR_IO_16_Pin); // PJ4
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_17_GPIO_Port, USR_IO_17_Pin); // PG1
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_18_GPIO_Port, USR_IO_18_Pin); // PF12
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_19_GPIO_Port, USR_IO_19_Pin); // PF11
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_20_GPIO_Port, USR_IO_20_Pin); // PE8
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_21_GPIO_Port, USR_IO_21_Pin); // PG0
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_22_GPIO_Port, USR_IO_22_Pin); // PE7
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_23_GPIO_Port, USR_IO_23_Pin); // PE9
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_24_GPIO_Port, USR_IO_24_Pin); // PE13
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_25_GPIO_Port, USR_IO_25_Pin); // PE11
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_26_GPIO_Port, USR_IO_26_Pin); // PE14
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_27_GPIO_Port, USR_IO_27_Pin); // PB10
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_28_GPIO_Port, USR_IO_28_Pin); // PE12
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_29_GPIO_Port, USR_IO_29_Pin); // PE15
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_30_GPIO_Port, USR_IO_30_Pin); // PH7
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_31_GPIO_Port, USR_IO_31_Pin); // PJ5
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_32_GPIO_Port, USR_IO_32_Pin); // PH8
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_33_GPIO_Port, USR_IO_33_Pin); // PB14
	  osDelay(10);
	  HAL_GPIO_TogglePin(USR_IO_34_GPIO_Port, USR_IO_34_Pin); // PA10
	  osDelay(10);
	  //Processed 96 lines.
  }
  /* USER CODE END 5 */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM7 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM7) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
