import csv

with open('pin_names.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    line_count = 0
    print(f'osDelay(100);')
    for row in csv_reader:
        print(f'HAL_GPIO_TogglePin({row[1]}_GPIO_Port, {row[1]}_Pin); // {row[0]}')
        print(f'osDelay(10);')
        line_count += 1
    print(f'//Processed {line_count} lines.')
